import requests
from bs4 import BeautifulSoup
import sys
import getopt

all_ships = {}
all_columns = {}

def parse_page(soup):
    attributes = {}
    header_names = soup.find_all("h1")
    shipName =  header_names[0].text.encode('ascii','ignore');
    print 'Name: ', shipName
    header_data = soup.find_all("h2")

    allProperties = soup.find_all("div", {"class": "fieldgroup"})
    if (allProperties):
        for properties in allProperties:
            try:
                titles = properties.find_all('h2')
                propertyElements = properties.find_all("div", {"class": "field-label-inline-first"});
                if (propertyElements):
                    for element in propertyElements:
                        try:
                            tag = element.text.encode('ascii','ignore')
                            if (element.next_sibling.next_sibling):
                                value = element.next_sibling.next_sibling.text.encode('ascii','ignore')
                            else:
                                value = element.next_sibling.encode('ascii','ignore')
                            tagStr = str(tag).strip();
                            attributes[tagStr] = str(value).strip()
                            all_columns[tagStr] = tagStr;

                        except:
                            pass
            except:
                pass

    all_ships[shipName] = attributes

def getEquipment(currentURL):
    r = requests.get(currentURL)
    soup = BeautifulSoup(r.content, 'html.parser')
    parse_page(soup)

def processPage(soup):
    equipments = soup.find_all("h2", {"class": "title"})

    for equipment in equipments:
        links = equipment.find_all("a")
        if (links):
            link = links[0].attrs['href']
            newUrl = 'https://www.dredgepoint.org/' + link
            getEquipment(newUrl);


def  getAllPages(baseURL, startPage, endPage):
    print 'BaseURL=', baseURL

    eNextPage = int(startPage) - 1
    iEndPage = int(endPage) - 1

    if (eNextPage == 0):
        nextPage = ''
    else:
        nextPage = '?page=' + str(eNextPage);
    index = eNextPage;

    while(True):
        currentURL = baseURL + nextPage;
        print 'CurrentURL=', currentURL;
        r = requests.get(currentURL);
        if (r):
            soup = BeautifulSoup(r.content, 'html.parser');
            processPage(soup)
            index +=1;
            if (index > iEndPage):
                break;
            nextPage = '?page=' + str(index);
        else:
            break;

if __name__ == '__main__':

    startPage = 0
    endPage = 1
    outputfile = 'dredgers.csv'

    try:
        opts, args = getopt.getopt(sys.argv[1:],"hs:e:o:")
    except getopt.GetoptError:
        print sys.args[0] + ' -s <startPage> -e <endPage>-o <outputfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print sys.argv[0] + ' -i <inputfile> -o <outputfile>'
            sys.exit()
        elif opt in ("-s"):
            startPage = arg
        elif opt in ("-e"):
            endPage = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    if (startPage < 0 and endPage < startPage):
        print 'Invalid start or end page numbers.   Exiting...'
        sys.exit(2)

    print 'Starting scraping...'

    baseURL = 'https://www.dredgepoint.org/dredging-database/equipment';
    getAllPages(baseURL, startPage, endPage)

    with open(outputfile + '.csv', 'w') as f:

        header = 'Vessel name;';
        for column in all_columns:
            header = header + column + ';'
        header += '\n'
        f.write(header)

        for name, ship in all_ships.iteritems():
            line = name + ';'
            for column in all_columns:
                try:
                    columnValue = ship[column]
                    line = line + columnValue + ';'
                except:
                    line = line + ';'
                    pass
            line += '\n'
            f.write(line)
        f.close

    print 'Finished scraping...'